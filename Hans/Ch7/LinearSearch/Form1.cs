﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LinearSearch
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        int[] a = { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26,
            28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50 };

        private void searchButton_Click(object sender, EventArgs e)
        {
            int searchKey = Int32.Parse(inputTextBox.Text);
            int elementIndex = LinearSearch(a, searchKey);
            if (elementIndex != -1)
                outputLabel.Text = "Found value in element " + elementIndex;
            else
                outputLabel.Text = "Value not found";
        }
        public int LinearSearch( int[] array, int key)
        {
            for( int n = 0; n < array.Length; n++)
            {
                if (array[n] == key)
                    return n;
            }
            return -1;
        }
    }
}
