﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Histogram
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            int[] n = { 19, 3, 15, 7, 11, 9, 13, 5, 17, 1 };
            string output = "";

            output += "Element\tvalue\tHistogram\n";

            for (int i = 0; i < n.Length; i++)
            {
                output += "\n" + i + "\t" + n[i] + "\t";

                for (int j = 1; j <= n[i]; j++)
                    output += "*";
            }

            MessageBox.Show(output, "Histogram Printing Program");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
