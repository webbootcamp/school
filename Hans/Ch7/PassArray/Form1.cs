﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PassArray
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void outputButton_Click(object sender, EventArgs e)
        {
            int[] a = { 1, 2, 3, 4, 5 };
            outputLabel.Text = "Effects of passing entire array " +
                "call-by-reference:\n\nThe values of the original " +
                "array are:\n\t";

            for (int i = 0; i < a.Length; i++)
                outputLabel.Text += "  " + a[i];

            ModifyArray(a);

            outputLabel.Text += "\n\nThe values of the modified array are:\n\t";

            for (int i = 0; i < a.Length; i++)
                outputLabel.Text += "  " + a[i];

            outputLabel.Text += "\n\nEffects of passing array " +
                "element call-by-value:\n\na[3] before " +
                "ModifyElement: " + a[3];

            ModifyElement(a[3]);

            outputLabel.Text +=
                "\na[3] after ModifyElement: " + a[3];

        }
        public void ModifyArray( int[] b )
        {
            for (int j = 0; j < b.Length; j++)
                b[j] *= 2;
        }
        public void ModifyElement ( int e )
        {
            outputLabel.Text +=
                "\nvalue received in ModifyElement: " + e;

            e *= 2;

            outputLabel.Text +=
                "\nvalue calculated in ModifyElement: " + e;
        }
    }
}
