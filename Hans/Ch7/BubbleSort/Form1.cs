﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BubbleSort
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int[] a = { 2, 6, 4, 8, 10, 12, 89, 68, 45, 37 };

            label1.Text = "Data items in original order\n";

            for (int i = 0; i < a.Length; i++)
                label1.Text += "  " + a[i];

            BubbleSort(a);

            label1.Text += "\n\nData items in ascending order\n";

            for (int i = 0; i < a.Length; i++)
                label1.Text += "  " + a[i];
        }
        public void BubbleSort(int[] b)
        {
            for (int pass = 1; pass < b.Length; pass++)

                for (int i = 0; i < b.Length - 1; i++)

                    if (b[i] > b[i + 1])
                        Swap(b, i);
                
        }
        public void Swap(int[] c, int first)
        {
            int hold;
            hold = c[first];
            c[first] = c[first + 1];
            c[first + 1] = hold;
        }
    }
}
