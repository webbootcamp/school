﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace maximumValue
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        double Maximum(double x, double y, double z)
        {
            return Math.Max(x, Math.Max(y, z));

        }

        private void button1_Click(object sender, EventArgs e)
        {
            double number1 = Double.Parse(textBox1.Text);
            double number2 = Double.Parse(textBox2.Text);
            double number3 = Double.Parse(textBox3.Text);

            double maximum = Maximum(number1, number2, number3);

            label4.Text = "maximum is: " + maximum;
        }
    }
}
