﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SqInt
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        int Square(int y)
        {
            return y * y;
        }
     

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "";
            for (int counter = 1; counter <= 10; counter++)
            {
                int result = Square(counter);

                label1.Text += "The square of " + counter +
                    " is " + result + "\n";
            }
        }
    }
}
