﻿namespace RollDie
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rollButton = new System.Windows.Forms.Button();
            this.dieLabel1 = new System.Windows.Forms.Label();
            this.dieLabel2 = new System.Windows.Forms.Label();
            this.dieLabel3 = new System.Windows.Forms.Label();
            this.dieLabel4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // rollButton
            // 
            this.rollButton.Location = new System.Drawing.Point(299, 25);
            this.rollButton.Name = "rollButton";
            this.rollButton.Size = new System.Drawing.Size(136, 38);
            this.rollButton.TabIndex = 0;
            this.rollButton.Text = "Roll\'em!";
            this.rollButton.UseVisualStyleBackColor = true;
            this.rollButton.Click += new System.EventHandler(this.rollButton_Click);
            // 
            // dieLabel1
            // 
            this.dieLabel1.AllowDrop = true;
            this.dieLabel1.AutoSize = true;
            this.dieLabel1.Location = new System.Drawing.Point(68, 77);
            this.dieLabel1.MinimumSize = new System.Drawing.Size(200, 200);
            this.dieLabel1.Name = "dieLabel1";
            this.dieLabel1.Size = new System.Drawing.Size(200, 200);
            this.dieLabel1.TabIndex = 1;
            // 
            // dieLabel2
            // 
            this.dieLabel2.AllowDrop = true;
            this.dieLabel2.AutoSize = true;
            this.dieLabel2.Location = new System.Drawing.Point(524, 77);
            this.dieLabel2.MinimumSize = new System.Drawing.Size(200, 200);
            this.dieLabel2.Name = "dieLabel2";
            this.dieLabel2.Size = new System.Drawing.Size(200, 200);
            this.dieLabel2.TabIndex = 2;
            this.dieLabel2.Click += new System.EventHandler(this.dieLabel2_Click);
            // 
            // dieLabel3
            // 
            this.dieLabel3.AllowDrop = true;
            this.dieLabel3.AutoSize = true;
            this.dieLabel3.Location = new System.Drawing.Point(68, 389);
            this.dieLabel3.MinimumSize = new System.Drawing.Size(200, 200);
            this.dieLabel3.Name = "dieLabel3";
            this.dieLabel3.Size = new System.Drawing.Size(200, 200);
            this.dieLabel3.TabIndex = 3;
            // 
            // dieLabel4
            // 
            this.dieLabel4.AllowDrop = true;
            this.dieLabel4.AutoSize = true;
            this.dieLabel4.Location = new System.Drawing.Point(524, 389);
            this.dieLabel4.MinimumSize = new System.Drawing.Size(200, 200);
            this.dieLabel4.Name = "dieLabel4";
            this.dieLabel4.Size = new System.Drawing.Size(200, 200);
            this.dieLabel4.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 676);
            this.Controls.Add(this.dieLabel4);
            this.Controls.Add(this.dieLabel3);
            this.Controls.Add(this.dieLabel2);
            this.Controls.Add(this.dieLabel1);
            this.Controls.Add(this.rollButton);
            this.Name = "Form1";
            this.Text = "Dice";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button rollButton;
        private System.Windows.Forms.Label dieLabel1;
        private System.Windows.Forms.Label dieLabel2;
        private System.Windows.Forms.Label dieLabel3;
        private System.Windows.Forms.Label dieLabel4;
    }
}

