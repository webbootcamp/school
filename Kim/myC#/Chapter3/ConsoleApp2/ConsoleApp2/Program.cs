﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {

            MessageBox.Show("Advanced Calculations. Press Ok to continue.");
            //Declare variables
            int x;

            //perform operations
            x = 7 + 3 * 6 / 2 - 1;

            Console.WriteLine("x is {0}.", x);
            x = 2 % 2 + 2 * 2 - 2 / 2;
            Console.WriteLine("x is now {0}.", x);
            x = (3 * 9 * (3 + (9 * 3 / (3))));
            Console.WriteLine("x is finally {0}.", x);

            Console.ReadLine();

        }
    }
}
