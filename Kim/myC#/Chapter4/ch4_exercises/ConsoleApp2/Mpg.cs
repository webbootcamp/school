﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Mpg
    {
        static void Main(string[] args)
        {
            //Declare variables
            double miles,
                   gallons,
                   mpg;

            //get user input, how many miles driven and gallons used for this tank
            Console.WriteLine("Enter miles driven: ");
            miles = Console.ReadLine();

            Console.WriteLine("Enter gallons used: ");
            gallons = Console.ReadLine();

            //get the miles per gallon for this trip
            // miles/gallons
            mpg = (miles / gallons);

            // output mpg
            
            Console.WriteLine("Your MPG for this trip is: {0}.");
            Console.ReadLine();

         }
    }
}
