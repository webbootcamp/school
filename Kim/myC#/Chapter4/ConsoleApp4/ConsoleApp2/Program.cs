﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ConsoleApp2
{
    class Bankcalculator
    {
        static void Main(string[] args)
        {
            int inputNum;

            string output;
            string input;

            Console.WriteLine("Enter 5 digit integer:  ");
            // ensure user is entering integers
            
            while (!int.TryParse(Console.ReadLine(), out inputNum) )
            {
                
                MessageBox.Show("Invalid input. Enter a 5-digit integer");
            }

            //We passed the integer test
            //now test to see if it the length is 5
            if (inputNum.ToString().Length != 5) 
            {
                MessageBox.Show("Invalid format. Requires 5 digits");
                                        
            }
            else
            {
                Console.WriteLine("Thank you.");
                //now determine if this is a palindrome
                
                input = inputNum.ToString();
                output = new string(input.ToCharArray().Reverse().ToArray());
                MessageBox.Show("Reverse of input string", output);
            }

            Console.ReadLine();
            
            

        }
    }
}
