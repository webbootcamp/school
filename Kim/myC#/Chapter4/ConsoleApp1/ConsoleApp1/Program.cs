﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class CreditApp
    {
        static void Main(string[] args)
        {
            //Declare variables

            int AccountNum;

            int   BeginBal,
                  EndBal,
                  TotCredits,
                  TotDebits;

            int CreditLimit = 5000;

            /* Have user input their account number, begin balance
               Total chares, and total payments and credits */
               
            Console.WriteLine("Enter your account number: ");
            AccountNum = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Enter your Beginning Balance: ");
            BeginBal = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Enter your Total Charges: ");
            TotDebits= Int32.Parse(Console.ReadLine());

            Console.WriteLine("Enter your Total Credits/Payments: ");
            TotCredits = Int32.Parse(Console.ReadLine());

            // Calculate the current balance
            EndBal = (BeginBal + TotDebits) - TotCredits;

            Console.WriteLine("Your current balance is: $ {0}", EndBal);
            if (EndBal > CreditLimit)
            {
                Console.WriteLine("You have exceeded your credit limit of {0}.", CreditLimit);
            }
            Console.ReadLine();

        }
    }
}
