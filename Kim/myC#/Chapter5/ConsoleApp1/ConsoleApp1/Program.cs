﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //factorial of 5, and display output in 3 columns
            

            // print colums
            Console.WriteLine("\tN Value\t" + "\tFact 32\t" + "\tFact 64\n");
            
             int i, fact;
             fact = 1;

            for (i = 5; i >= 1; i--)
            {
                fact = fact * i;
                //Console.WriteLine("fact: {0}", fact);
                //Console.WriteLine("\t{0}", i);
                Console.WriteLine($"\t{i}\t \t{fact}\t \t{fact}");
            }

            //out put binary equivalent from 1 - 256
            for (i = 0; i<= 256; i++)
            {
                GetBinary(i);
            }
            
            Console.ReadLine();

        }

        static void GetBinary (int num)
        {
            //outputs the binary equivalent of a number
            int input = num;
            int remainder;
            int quotient;
            string result = String.Empty;

            while (num > 0)
            {
                quotient  = num / 2;
                remainder = num % 2;
                result = remainder.ToString() + result;
                num = quotient;
                

            }
            Console.WriteLine("Binary of {0} is: {1}", input, result);

        }
    }
}
